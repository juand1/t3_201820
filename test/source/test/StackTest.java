package test;

import static org.junit.Assert.*;
import java.util.Iterator;
import model.data_structures.Stack;

import org.junit.Before;
import org.junit.Test;

public class StackTest 
{
	private Stack <String> stack;
	
	@Before
    public void setupEscenario1( )
    {
        stack = new Stack<String>();
        stack.push("A");
        stack.push("L");
        stack.push("O");
        stack.push("H");
    }
    @Test
    public void testIsEmpty()
    {
    	setupEscenario1( );
    	assertFalse("La lista no deberia ser vacia",stack.isEmpty());
    }
    @Test
    public void testSize()
    {
    	setupEscenario1( );
    	assertEquals("La lista no tiene el tamaño adecuado", 4,stack.size());
    }
    @Test
    public void testPush() 
	{
    	setupEscenario1( );
    	stack.push("M");
    	Iterator<String> iter=stack.iterator();
    	String a=iter.next();
    	assertEquals("El objeto guardado no era el adecuado", "M",a);
	}
    @Test
    public void testPop() 
	{
    	setupEscenario1( );
    	assertEquals("El ultimo elemento no es el correcto", "H",stack.pop());
	}
    @Test
    public void testIterator() 
	{
    	setupEscenario1( );
		Iterator<String> iter=stack.iterator();
		String a=iter.next();
		assertEquals("El elemento no es el correcto", "H",a);
		a=iter.next();
		assertEquals("El elemento no es el correcto", "O",a);
		a=iter.next();
		assertEquals("El elemento no es el correcto", "L",a);
		a=iter.next();
		assertEquals("El elemento no es el correcto", "A",a);
	}
	
}
