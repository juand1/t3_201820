package test;

import model.data_structures.DoubleLinkedList;


import org.junit.Test;
import static org.junit.Assert.*;
import java.util.Iterator;

public class DoubleLinkedListTest
{
	private DoubleLinkedList<String> lista;
	
	public void setupEscenario1( )
    {
        lista=new DoubleLinkedList<String>();
        lista.add("H");
        lista.add("O");
        lista.add("L");
        lista.addAtEnd("A");
    }
	
	@Test
	public void testIterator()
	{
		setupEscenario1();
		Iterator<String> iter=lista.iterator();
		String actual=iter.next();
		assertEquals("EL elemento no es el adecuado","L",actual);
		actual=iter.next();
		assertEquals("EL elemento no es el adecuado","O",actual);
		actual=iter.next();
		assertEquals("EL elemento no es el adecuado","H",actual);
		actual=iter.next();
		assertEquals("EL elemento no es el adecuado","A",actual);
	}
	
	@Test
	public void testGetSize()
	{
		setupEscenario1();
		assertEquals("El tamano no es el correcto",4,lista.getSize(),0);
	}
	
	@Test
	public void testAdd()
	{
		setupEscenario1();
		lista.add("X");
		assertEquals("EL elemento no es el adecuado","X",lista.iterator().next());
	}
	
	
	
	@Test
	public void testGetCurrentElement()
	{
		setupEscenario1();
		assertEquals("EL elemento no es el adecuado","H",lista.getCurrentElement());
	}
	
	@Test
	public void testGetElement()
	{
		setupEscenario1();
		assertEquals("EL elemento no es el adecuado",null,lista.getElement("R"));
		assertEquals("EL elemento no es el adecuado","H",lista.getElement("H"));
	}
	

	
	@Test
	public void testNext()
	{
		setupEscenario1();
		assertEquals("EL elemento no es el adecuado","A",lista.next());
	}
	
	@Test
	public void testPrevious()
	{
		setupEscenario1();
		assertEquals("EL elemento no es el adecuado","O",lista.previous());
	}
	
	@Test
	public void testHasNext()
	{
		setupEscenario1();
		assertTrue("EL elemento debe tener siguiente",lista.hasNext());
	}
	

	
}
