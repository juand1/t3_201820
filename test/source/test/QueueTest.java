package test;

import static org.junit.Assert.*;
import java.util.Iterator;

import model.data_structures.Queue;


import org.junit.Before;
import org.junit.Test;

public class QueueTest 
{
	private Queue <String> que;
	
	@Before
    public void setupEscenario1( )
    {
        que = new Queue<String>();
        que.enqueue("H");
        que.enqueue("O");
        que.enqueue("L");
        que.enqueue("A");
    }
    @Test
    public void testIsEmpty()
    {
    	setupEscenario1( );
    	assertFalse("La lista no deberia ser vacia",que.isEmpty());
    }
    @Test
    public void testSize()
    {
    	setupEscenario1( );
    	assertEquals("La lista no tiene el tamaño adecuado", 4,que.size());
    }
    @Test
    public void testEnqueue() 
	{
    	setupEscenario1( );
    	
    	assertEquals("El objeto guardado no era el adecuado", "H",que.darPrimero());
	}
    @Test
    public void testDequeue() 
	{
    	setupEscenario1( );
    	que.dequeue();
    	assertEquals("El primer elemento no es el correcto", "O",que.darPrimero());
	}
    @Test
    public void testIterator() 
	{
    	setupEscenario1( );
		Iterator<String> iter=que.iterator();
		String a=iter.next();
		assertEquals("El elemento no es el correcto", "H",a);
		a=iter.next();
		assertEquals("El elemento no es el correcto", "O",a);
		a=iter.next();
		assertEquals("El elemento no es el correcto", "L",a);
		a=iter.next();
		assertEquals("El elemento no es el correcto", "A",a);
	}
	
}