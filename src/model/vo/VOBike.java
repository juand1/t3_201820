package model.vo;

/**
 * Representation of a byke object
 */
public class VOBike 
{

private int bikeId;
	
	public VOBike(int pBikeId)
	{
		bikeId=pBikeId;
	}

	/**
	 * @return id_bike - Bike_id
	 */
	public int id() 
	{
		return bikeId;
	}	
}
