package model.vo;

/**
 * Representation of a Trip object
 */
public class VOTrip {

	
private int tripId;
	
	private String startTime;
	
	private String endTime;
	
	private VOBike bikeId;
	
	private int tripSeconds;
	
	private int originStationId;
	
	private String originStationName;
	
	private int destinationStationId;
	
	private String destinationStation;	
	
	private String userType;
	
	private String gender;
	
	private String birthYear;
	
	

	public VOTrip(int pId,String pStartTime,String pEndTime,int pBikeId, int pTripSeconds,int pOriginId, 
			String pOrigin, int pDestinationId, String pDestination, String pUser, String pGender, String pBirth )
	{
		tripId=pId;
		startTime=pStartTime;
		endTime=pEndTime;
		bikeId=new VOBike(pBikeId);
		tripSeconds=pTripSeconds;
		originStationName=pOrigin;
		destinationStation=pDestination;
		originStationId=pOriginId;
		destinationStationId=pDestinationId;
		userType=pUser;
		gender=pGender;
		birthYear=pBirth;
	}
	/**
	 * @return id - Trip_id
	 */
	public int getTripId() 
	{
		return tripId;
	}
	
	/**
	 * @return startTime
	 */
	public String getTtartTime() 
	{
		return startTime;
	}
	
	/**
	 * @return endTime
	 */
	public String getEndTime() 
	{
		return endTime;
	}	
	
	/**
	 * @return Bike
	 */
	public VOBike getByke() 
	{
		return bikeId;
	}	
	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() 
	{
		return tripSeconds;
	}

	/**
	 * @return station_name - Origin Station Name .
	 */
	public String getFromStationName() 
	{
		return originStationName;
	}
	
	/**
	 * @return station_id - Origin Station id .
	 */
	public int getFromStationId() 
	{
		return originStationId;
	}
	
	/**
	 * @return station_name - Destination Station Name .
	 */
	public String getToStationName() 
	{
		return destinationStation;
	}
	
	/**
	 * @return station_id - Destination Station id .
	 */
	public int getToStationId() 
	{
		return destinationStationId;
	}
	
	/**
	 * @return userType .
	 */
	public String getUser() 
	{
		return userType;
	}
	
	/**
	 * @return genderType .
	 */
	public String getGender() 
	{
		return gender;
	}
	
	/**
	 * @return birthYear.
	 */
	public String getBirthYear() 
	{
		return birthYear;
	}
}
