package model.vo;

public class VOStation implements Comparable<VOStation>
{
	private int stationId;
	
	private String name;
	
	private String city;
	
	private double latitude;
	
	private double longitude;
	
	private int dpCapacity;
	
	private String onlineDate;
	
	public VOStation(int pStationId,String pName, String pCity, double pLat, double pLong, int pDpCapacity, String pOnlineDate)
	{
		stationId=pStationId;
		name=pName;
		city=pCity;
		latitude=pLat;
		longitude=pLong;
		dpCapacity=pDpCapacity;
		onlineDate=pOnlineDate;
	}
	
	public int getStationId()
	{
		return stationId;
	}
	
	public String getName()
	{
		return name;
	}
	
	public String getCity()
	{
		return city;
	}
	
	public double getLatitude()
	{
		return latitude;
	}
	
	public double getLongitude()
	{
		return longitude;
	}
	
	public int getDpCapacity()
	{
		return dpCapacity;
	}
	
	public String getOnlineDate()
	{
		return onlineDate;
	}

	public int compareTo(VOStation o) 
	{
		return stationId-o.stationId;
	}

}
