package model.data_structures;

import java.util.Iterator;

public class Stack<T> implements IStack<T>
{
	// -----------------------------------------------------------------
    // Atributos
    // -----------------------------------------------------------------
	private NodeS<T> primero;
	
	private int size;
	// -----------------------------------------------------------------
    // Clases privadas
    // -----------------------------------------------------------------
	
	@SuppressWarnings("hiding")
	private class NodeS<T>
	{
		private NodeS<T> siguiente;
		
		private T objeto;
		
		public NodeS(T pObj)
		{
			objeto=pObj;
			siguiente=null;
		}
		public T darObjeto()
		{
			return objeto; 
		}
		public void asignarSiguiente(NodeS<T> pNodo)
		{
			siguiente=pNodo;
		}
		public NodeS<T> darSiguiente()
		{
			return siguiente;
		}
	}
	/**
     * Crea una un list iterator que implementa la interfaz iterador.
     * <b>post: </b> Se creo un iterador..
     */
	private class ListIterator implements Iterator<T>
    {
        private NodeS <T> current = primero;
        
        public boolean hasNext()
        {  
        	return current != null;  
        }
        public void remove() 
        { 
        	
        }
        public T next()
        {
        	T item = current.darObjeto();
        	current = current.darSiguiente();
        	return item;
        } 
    }
	// -----------------------------------------------------------------
    // Constructores
    // -----------------------------------------------------------------
	
	public Stack()
	{
		primero=null;
		size=0;
	}
	
	// -----------------------------------------------------------------
    // Métodos
    // -----------------------------------------------------------------
	/**
	 * Retorna true si la Pila esta vacia
	 * @return true si la Pila esta vacia, false de lo contrario
	 */
	public boolean isEmpty() 
	{
		return size==0;
	}
	
	/**
	 * Retorna el numero de elementos contenidos
	 * @return el numero de elemntos contenidos
	 */
	public int size() 
	{
		return size;
	}

	
	/**
	 * Inserta un nuevo elemento en la Pila
	 * @param t el nuevo elemento que se va ha agregar
	 */
	public void push(T t) 
	{
		NodeS<T> nuevo=new NodeS<T>(t);
		nuevo.asignarSiguiente(primero);
		primero=nuevo;
		size++;
	}
	
	
	/**
	 * Quita y retorna el elemento agregado más recientemente
	 * @return el elemento agregado más recientemente
	 */
	public T pop() 
	{
		if(primero==null)
			return null;
		else
		{
			NodeS<T> aDevolver=primero;
			primero=primero.siguiente;
			size--;
			return aDevolver.objeto;
		}
	}

    /**
     * Retorna un iterador.
     * @return El iterador.
     */
	public Iterator<T> iterator() 
	{
		return new ListIterator();
	}
	
}
