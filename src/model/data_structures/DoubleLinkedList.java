package model.data_structures;

import java.util.Collections;
import java.util.Iterator;
import java.util.NoSuchElementException;


public class DoubleLinkedList<T extends Comparable<T>> implements IDoublyLinkedList<T>
{
	// -----------------------------------------------------------------
    // Atributos
    // -----------------------------------------------------------------

    /**
     * Primer nodo.
     */
	private Node<T> primero;
	/**
     * Nodo actual.
     */
	private Node<T> actual;
	/**
     * Tamanño lista.
     */
	private int tamanoLista;
	// -----------------------------------------------------------------
    // Constructores
    // -----------------------------------------------------------------

    /**
     * Crea una lista doblemente enlazada.
     * <b>post: </b> Se creó un carro una listaa doblemente enlazada.
     */
	public DoubleLinkedList()
	{
		primero=null;
		actual=null;
		tamanoLista=0;
	}
	
	private class Node<T>
	{
		private Node<T> siguiente;
		
		private Node<T> anterior;
		
		private T objeto;
		
		public Node(T pObj)
		{
			objeto=pObj;
			siguiente=null;
			anterior=null;
		}
		public T darObjeto()
		{
			return objeto; 
		}
		public void asignarSiguiente(Node<T> pNodo)
		{
			siguiente=pNodo;
		}
		public void asignarAnterior(Node<T> pNodo)
		{
			anterior=pNodo;
		}
		public Node<T> darSiguiente()
		{
			return siguiente;
		}
		public Node<T> darAnterior()
		{
			return anterior;
		}
		public boolean hasNext()
		{
			return siguiente!=null;
		}
		public boolean hasPrevious()
		{
			return anterior!=null;
		}
		
	}
	
	// -----------------------------------------------------------------
    // Clases privadas
    // -----------------------------------------------------------------
	/**
     * Crea una un list iterator que implementa la interfaz iterador.
     * <b>post: </b> Se creo un iterador..
     */
	private class ListIterator implements Iterator<T>
    {
        private Node <T> current = primero;
        public boolean hasNext()
        {  
        	return current != null;  
        }
        public void remove() 
        { 
        	
        }
        public T next()
        {
        	T item = current.darObjeto();
        	current = current.darSiguiente();
        	return item;
        } 
    }
	 // -----------------------------------------------------------------
    // Métodos
    // -----------------------------------------------------------------

    /**
     * Retorna un iterador.
     * @return El iterador.
     */
	public Iterator<T> iterator()
    {  
		return new ListIterator();  
	}
	/**
     * Retorna el tamano de la lista.
     * @return El iterador.
     */
	public Integer getSize() 
	{
		return tamanoLista;
	}
	/**
     * Crea un nuevo nodo al principio de la lista.
     * @param obj El nuevo objeto del nuevo nodo.
     */
	public void add(T obj) 
	{
		if(primero==null)
		{
			primero=new Node<T>(obj);
		}
		else
		{
			Node <T> nuevo=new Node<T>(obj);
			nuevo.asignarSiguiente(primero);
			primero.asignarAnterior(nuevo);
			primero=nuevo;
		}
		tamanoLista++;
	}
	/**
     * Crea un nuevo nodo al final de la lista.
     * @param obj El nuevo objeto del nuevo nodo.
     */
	public void addAtEnd(T obj) 
	{
		if(primero==null)
		{
			primero=new Node<T>(obj);
		}
		else
		{
			actual=primero;
			while(actual.hasNext())
			{
				actual=actual.darSiguiente();
			}
			Node<T> nuevo=new Node<T>(obj);
			actual.asignarSiguiente(nuevo);
			nuevo.asignarAnterior(actual);
		}
		tamanoLista++;
	}
	/**
     * Crea un nuevo nodo al en la posicion de la lista indicada de la lista o de ser muy grande lo añade al final.
     * @param obj El nuevo objeto del nuevo nodo.
     * @param pos posicion en la que se quiere añadir el objeto
     */
	public void AddAtK(int pos, T obj) 
	{
		if(pos>tamanoLista-1)
		{
			addAtEnd(obj);
		}
		else
		{	
			Node<T> nuevo=new Node<T>(obj);
			int posActual=0;
			actual=primero;
			while(posActual!=pos)
			{
				actual=actual.darSiguiente();
				posActual++;
			}
			if(pos==0)
			{
				nuevo.asignarSiguiente(primero);
				primero.asignarAnterior(nuevo);
				primero=nuevo;
			}
			else
			{
				nuevo.asignarSiguiente(actual);
				nuevo.asignarAnterior(actual.darAnterior());
				actual.darAnterior().asignarSiguiente(nuevo);
				actual.asignarAnterior(nuevo);
			}
		}
		tamanoLista++;
	}
	/**
     * Busca el objeto que se pasa por parametro.
     * @param obj El objeto que se quiere buscar.
     * @return El objeto equivalente 
     */
	public T getElement(T obj) 
	{
		actual=primero;
		T resp=null;
		boolean encontrado=false;
		while(actual!=null && !encontrado)
		{
			if(obj.compareTo(actual.darObjeto())==0)
			{
				encontrado=true;
				resp=actual.darObjeto();
			}
			actual=actual.darSiguiente();
		}
		return resp;
	}

	/**
     * Retorna el objeto del elemento actual.
     * @return El objeto del elemento actual.
     */
	public T getCurrentElement() 
	{
		return actual.darObjeto();
	}

	/**
     * Elimina nodo de lista con objeto pasado
     * @param obj objeto que se desea eliminar
     */
	public void delete(T obj) 
	{
		actual=primero;
		if(actual!=null)
		{
			boolean encontrado=false;
			while(actual!=null && !encontrado)
			{
				if(actual.darObjeto().compareTo(obj)==0)
				{
					encontrado=true;
				}
			}
			if(actual!=null)
			{
				if(actual==primero)
				{
					primero=primero.darSiguiente();
					if(primero!=null)
						primero.asignarAnterior(null);
				}
				else if(actual.darSiguiente()==null)
				{
					actual.darAnterior().asignarSiguiente(null);
				}
				else
				{
					actual.darAnterior().asignarSiguiente(actual.darSiguiente());
					actual.darSiguiente().asignarAnterior(actual.darAnterior());
				}
				actual=primero;
				tamanoLista--;
			}
		}
		
	}

	/**
     * Elimina objeto en la posicon pos de existir.
     * @param pos posicion de nodo que se desea eliminar
     */
	public void deleteAtK(int pos) 
	{
		if(pos>tamanoLista-1)
		{
		}
		else
		{
			actual=primero;
			int posActual=0;
			while(posActual!=pos)
			{
				actual=actual.darSiguiente();
				posActual++;
			}
			if(pos==0)
			{
				primero=primero.darSiguiente();
				if(primero!=null)
					primero.asignarAnterior(null);
			}
			else if(pos==tamanoLista)
			{
				actual.darAnterior().asignarSiguiente(null);
			}
			else
			{
				actual.darAnterior().asignarSiguiente(actual.darSiguiente());
				actual.darSiguiente().asignarAnterior(actual.darAnterior());
			}
			actual=primero;
			tamanoLista--;
		}
		
	}

	/**
     * Retorna el siguiente objeto del nodo actual.
     * @return El siguiente objeto del nodo actual.
     * @throws Cuando no existe tan elemento
     */
	public T next()  throws NoSuchElementException
	{
		if(actual==null)
		{
			actual=primero;
		}
		else if(!actual.hasNext())
		{
			throw new NoSuchElementException();
		}
		else
		{
			actual=actual.darSiguiente();
		}
		return actual.darObjeto();
	}

	/**
     * Retorna el anterior objeto del nodo actual.
     * @return El anterior objeto del nodo actual.
     * @throws Cuando no existe tan elemento
     */
	public T previous() throws NoSuchElementException
	{
		if(actual==null)
		{
			actual=primero;
		}
		else if(!actual.hasPrevious())
		{
			throw new NoSuchElementException();
		}
		else
		{
			actual=actual.darAnterior();
		}
		return actual.darObjeto();
	}

	/**
     * Retorna si el nodo actual tiene siguiente.
     * @return Si actual tiene siguiente.
     */
	public boolean hasNext() 
	{
		if(actual!=null)
		{
			return actual.hasNext();
		}
		else 
		{
			return false;
		}
	}

	/**
     * Retorna el primero nodo.
     * @return el primero nodo.
     */
	public Node<T> darPrimero()
	{
		return primero;
	}
}
