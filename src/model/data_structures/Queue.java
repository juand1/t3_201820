package model.data_structures;

import java.util.Iterator;



public class Queue <T>   implements IQueue<T>
{
	
	private NodeQ<T> primero;

	private int tamano;
	
	private NodeQ<T> ultimo;
	
	@SuppressWarnings("hiding")
	private class NodeQ<T>
	{
		private NodeQ<T> siguiente;
		
		private T objeto;
		
		public NodeQ(T pObj)
		{
			objeto=pObj;
			siguiente=null;
		}
		
		public T darObjeto()
		{
			return objeto; 
		}
		
		public void asignarSiguiente(NodeQ<T> pNodo)
		{
			siguiente=pNodo;
		}
		public NodeQ<T> darSiguiente()
		{
			return siguiente;
		}
	}
	/**
     * Crea una un list iterator que implementa la interfaz iterador.
     * <b>post: </b> Se creo un iterador..
     */
	private class ListIterator implements Iterator<T>
    {
        private NodeQ <T> current = primero;
        
        public boolean hasNext()
        {  
        	return current != null;  
        }
        public void remove() 
        { 

        }
        public T next()
        {
        	T item = current.darObjeto();
        	current = current.darSiguiente();
        	return item;
        } 
    }
	
	
	 
	
	public Iterator<T> iterator() 
	{
		return new ListIterator();
	}
	public Queue() 
	{
		primero=null;
		ultimo=null;
		tamano=0;
	}
	
	
	/**
	 * Retorna true si la Cola esta vacia
	 * @return true si la Cola esta vacia, false de lo contrario
	 */
	public boolean isEmpty() 
	{
		if (size()==0) 
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	

	/**
	 * Retorna el numero de elementos contenidos
	 * @return el numero de elemntos contenidos
	 */
	public int size() 
	{
		return tamano;
	}
	
	/**
	 * Inserta un nuevo elemento en la Cola
	 * @param t el nuevo elemento que se va ha agregar
	 */
	public void enqueue(T t) 
	{
		NodeQ<T> nuevo=new NodeQ<T>(t);
		if (isEmpty()==true) 
		{
			primero= nuevo;
			ultimo=primero;
		}
		else 
		{
			ultimo.asignarSiguiente(nuevo);
			ultimo=nuevo;
		}
		tamano++;
	}
	
	/**
	 * Quita y retorna el elemento agregado menos recientemente
	 * @return el elemento agregado menos recientemente
	 */
	public T dequeue() 
	{
		if (primero==null) 
		{
			return null;
		}
		else 
		{
		NodeQ<T> temp=primero.darSiguiente();
		NodeQ<T> retornar=primero;
		primero=temp;
		T obj=retornar.darObjeto();
		tamano--;
		return obj;
		}
	}
	
	public T darPrimero() {
		return primero.darObjeto();
	}
	
}

