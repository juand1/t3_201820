package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Iterator;

import api.IDivvyTripsManager;
import model.vo.VOStation;
import model.vo.VOTrip;
import model.data_structures.DoubleLinkedList;
import model.data_structures.IDoublyLinkedList;
import model.data_structures.Queue;
import model.data_structures.Stack;

public class DivvyTripsManager implements IDivvyTripsManager 
{
	private Stack<VOTrip> tripsS;
	
	private Stack <VOStation> stationsS;
	
	private Queue<VOTrip> tripsQ;
	
	private Queue <VOStation> stationsQ;
	
	public void loadStations (String stationsFile) 
	{
	
		BufferedReader br=null;
		String line="";
		stationsS=new Stack<VOStation>();
		stationsQ=new Queue<VOStation>();
		try
		{
			br=new BufferedReader(new FileReader(stationsFile));
			line=br.readLine();
			line=br.readLine();
			
			while(line!=null)
			{
		
				String[] inf=line.split(",");
				VOStation nuevo=new VOStation(Integer.parseInt(inf[0]),inf[1],inf[2],
						Double.parseDouble(inf[3]),Double.parseDouble(inf[4]),Integer.parseInt(inf[5]),inf[6]);
				stationsS.push(nuevo);
				
				stationsQ.enqueue(nuevo);
				
				line=br.readLine();
				
			}
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				br.close();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public void loadTrips (String tripsFile) 
	{
		BufferedReader br=null;
		String line="";
		tripsS=new Stack<VOTrip>();
		tripsQ=new Queue<VOTrip>();
		try
		{
			br=new BufferedReader(new FileReader(tripsFile));
			line=br.readLine();
			line=br.readLine();
			while(line!=null)
			{
				line=line.replace("\"", "");
				if(line.endsWith(","))
				{
					line=line+"N/A";
				}
				String[] inf=line.split(",");
				int tripId=Integer.parseInt(inf[0]);
				String startTime=inf[1];
				String endTime=inf[2];
				int bykeId=Integer.parseInt(inf[3]);
				int tripSeconds=Integer.parseInt(inf[4]);
				int originId=Integer.parseInt(inf[5]);
				String originName=inf[6];
				int destinationId=Integer.parseInt(inf[7]);
				String destinationName=inf[8];
				String userType=inf[9];
				String gender=inf[10];
				String birthYear=inf[11];
				VOTrip viaje=new VOTrip(tripId,startTime, endTime, bykeId, tripSeconds, originId, 
						originName, destinationId,destinationName, userType,gender, birthYear);
				tripsS.push(viaje);

				tripsQ.enqueue(viaje);
				
				line=br.readLine();
				
			}
			br.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				br.close();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public IDoublyLinkedList <String> getLastNStations (int bicycleId, int n) 
	{
		
		DoubleLinkedList retonar= new DoubleLinkedList();
		
		if(tripsQ==null)
		{
			loadTrips("./data/Divvy_Trips_2017_Q3.csv");
		}
	
		Iterator<VOTrip> iter=tripsQ.iterator();
	
		VOTrip actual=tripsQ.darPrimero();
	
		
		boolean termino=false;
		int cont=0;
	
		while(actual!=null && !termino)
		{
		
			if (actual.getByke().id()==bicycleId) 
			{
				cont++;
				retonar.add(actual.getToStationName());
				if (cont==n) {
					termino=true;
				}
			}
			if (iter.hasNext()) 
			{
			actual=iter.next();
			}
			else 
			{
				break;
			}
		}
		
		return retonar;

	}

	
	@Override
	public VOTrip customerNumberN (int stationID, int n) 
	{
		if(tripsS==null)
		{
			loadTrips("./data/Divvy_Trips_2017_Q3.csv");
		}
		VOTrip resp=null;
		Iterator<VOTrip> iter=tripsS.iterator();
		VOTrip actual=iter.next();
		int contadorViajes=0;
		boolean termino=false;
		while(actual!=null && !termino)
		{
			if(actual.getToStationId()==stationID)
				contadorViajes++;
			if(contadorViajes==n)
			{
				resp=actual;
				termino=true;
			}
			actual=iter.next();
		}
		return resp;
	}	


}
